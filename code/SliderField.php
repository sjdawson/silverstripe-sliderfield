<?php

/**
 * SliderField
 *
 * SliderField allows the client to slide to select a value from a pre-defined range
 * without having to resort to unsightly dropdowns or relying on NumericField etc.
 *
 * Utilisies the jQuery version of the library "noUiSlider"
 * http://refreshless.com/nouislider/
 *
 * @author Steven Dawson <steven@djdawson.co.uk>
 *
 * @todo Utilise all options of noUiSlider
 * @todo Option to reveal the fields that data will be stored into for manual entry (useful for big ranges where accuracy is still important)
 * @todo Enable support for 2 handles
 * @todo Refactor the template to remove inline script. Use .data attributes instead
 *
 */
class SliderField extends FormField
{
    /**
     * A range of options that the field will use to be displayed
     */
    protected $lower, $upper, $start, $handles, $connect, $orientation, $step, $value_prefix, $value_suffix;



    /**
     * Construct the field based on the options given
     */
    public function __construct($name, $title = null, $value = null, $options = null)
    {
        $options = $this->setOptions($options);

        parent::__construct($name, $title, $value, $options);
    }



    /**
     * Set class variables for use in the template, to those specified by the user
     */
    public function setOptions($options = array())
    {
        $this->lower 			= (isset($options['lower']))		?	$options['lower']			:1;
        $this->upper 			= (isset($options['upper']))		?	$options['upper']			:10;
        $this->handles 			= (isset($options['handles']))		?	$options['handles']			:1;
        $this->connect 			= (isset($options['connect']))		?	$options['connect']			:'lower';
        //$this->orientation 		= (isset($options['orientation']))	?	$options['orientation']		:'horizontal';
        $this->step 			= (isset($options['step']))			?	$options['step']			:false;
        $this->start 			= (isset($options['start']))		?	$options['start']			:0;
        $this->value_prefix 	= (isset($options['value_prefix']))	?	$options['value_prefix']	:'';
        $this->value_suffix 	= (isset($options['value_suffix']))	?	$options['value_suffix']	:'';
    }



    /**
     * Add any additional scripts and stylesheets we may need to the admin section when called
     */
    public function requirements()
    {
        // Javascript
        Requirements::set_write_js_to_body(true);
        Requirements::javascript("SliderField/js/jquery.nouislider.js");
        Requirements::set_write_js_to_body(false);

        // CSS
        Requirements::css("SliderField/css/jquery.nouislider.css");
    }



    /**
     * Scaffold the field in the usual SilverStripe fashion
     */
    public function Field($properties = array())
    {
        $this->requirements();
        $obj = ($properties) ? $this->customise($properties) : $this;

        return $obj->renderWith($this->getTemplates());
    }
}
