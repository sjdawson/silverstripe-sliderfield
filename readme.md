# Silverstripe SliderField #
---
This is a CMS field for use with Silverstripe 3.x, rather than give the user a standard number field and ask them to enter a number, you can show them a slider, with a pre-set range and other choices available.

---
### Requirements ###
* Silverstripe Version 3.x +

---
### Dependencies ###
This project makes use of the excellent "[NoUiSlider](http://refreshless.com/nouislider/)" by [Leon Gersen](https://twitter.com/LeonGersen) to craft the slider. 